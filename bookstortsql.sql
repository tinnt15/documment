USE [BookStore]
GO
/****** Object:  Table [dbo].[Author]    Script Date: 02/01/2018 1:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Author](
	[AuthorID] [int] IDENTITY(1,1) NOT NULL,
	[AuthorName] [nvarchar](100) NULL,
	[History] [nvarchar](max) NULL,
 CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED 
(
	[AuthorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Book]    Script Date: 02/01/2018 1:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[BookID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[CateID] [int] NULL,
	[AuthorID] [int] NULL,
	[PubID] [int] NULL,
	[Summary] [nvarchar](max) NULL,
	[ImgUrl] [nvarchar](max) NULL,
	[Price] [float] NULL,
	[Quantity] [float] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedDate] [date] NULL,
	[IsActive] [bit] NULL,
	[ISBN] [nvarchar](50) NULL,
	[ISBN13] [nvarchar](50) NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[BookID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 02/01/2018 1:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CateID] [int] IDENTITY(1,1) NOT NULL,
	[CateName] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comment]    Script Date: 02/01/2018 1:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[BookID] [int] NULL,
	[Content] [nvarchar](max) NULL,
	[CreatedDate] [date] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Publisher]    Script Date: 02/01/2018 1:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Publisher](
	[PubID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Publisher] PRIMARY KEY CLUSTERED 
(
	[PubID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 02/01/2018 1:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](max) NULL,
	[Email] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Author] ON 

INSERT [dbo].[Author] ([AuthorID], [AuthorName], [History]) VALUES (1, N'Cassandra Clare', N'Cassandra Clare is the author of City of Bones, the first book in the Mortal Instruments trilogy and a New York Times bestseller. She was born overseas and spent her early years traveling around the world with her family and several trunks of books. Cassandra lives in Brooklyn with her boyfriend, their two cats, and these days, even more books.')
INSERT [dbo].[Author] ([AuthorID], [AuthorName], [History]) VALUES (2, N'Lauren Kate', N'Lauren Kate recently finished her M.A. in Creative Writing at UC Davis, where she also teaches. She lives and writes in an old farm house in Winters, California. Her first novel, The Betrayal of Natalie Hargrove goes on sale one month before Fallen.')
INSERT [dbo].[Author] ([AuthorID], [AuthorName], [History]) VALUES (3, N'Alexandra Adornetto', N'Now seventeen, Alexandra Adornetto was fourteen when she published her first book, The Shadow Thief, in Australia. The daughter of two English teachers, she admits to being a compulsive book buyer who has run out of shelf space, and now stacks her reading “in wobbly piles on my bedroom floor.” Alex lives in Melbourne, Australia; Halo marks her U.S. debut.')
INSERT [dbo].[Author] ([AuthorID], [AuthorName], [History]) VALUES (4, N'Augusten Burroughs', N'When Augusten Burroughs released 2002''s Running with Scissors -- his memoir about growing up in the mother of all dysfunctional families -- readers didn''t know whether to drop their jaws in horror or hold their stomachs from laughing. Whatever reactions he gets from readers, Burroughs''s gift for dishing on all things stranger than fiction has made him a bestselling author.')
SET IDENTITY_INSERT [dbo].[Author] OFF
SET IDENTITY_INSERT [dbo].[Book] ON 

INSERT [dbo].[Book] ([BookID], [Title], [CateID], [AuthorID], [PubID], [Summary], [ImgUrl], [Price], [Quantity], [CreatedDate], [ModifiedDate], [IsActive], [ISBN], [ISBN13]) VALUES (3, N'
Add to Wish List
Format:Paperback
Release Date:June 2003
Length:320 Pages
More Details
Running with Scissors: A Memoir', 1, 4, 1, N'The #1 New York Times BestsellerAn Entertainment Weekly Top Ten Book of the YearNow a Major Motion Picture Running with Scissors is the true story of a boy whose mother (a poet with delusions of Anne Sexton) gave him away to be raised by her unorthodox psychiatrist who bore a striking resemblance to Santa Claus. So at the age of twelve, Burroughs found himself amidst Victorian squalor living with the doctor''s bizarre family, and befriending a pedophile who resided in the backyard shed. The story of an outlaw childhood where rules were unheard of, and the Christmas tree stayed up all year round, where Valium was consumed like candy, and if things got dull an electroshock- therapy machine could provide entertainment. The funny, harrowing and bestselling account of an ordinary boy''s survival under the most extraordinary circumstances.Running with Scissors AcknowledgmentsGratitude doesn''t begin to describe it: Jennifer Enderlin, Christopher Schelling, John Murphy, Gregg Sullivan, Kim Cardascia, Michael Storrings, and everyone at St. Martin''s Press. Thank you: Lawrence David, Suzanne Finnamore, Robert Rodi, Bret Easton Ellis, Jon Pepoon, Lee Lodes, Jeff Soares, Kevin Weidenbacher, Lynda Pearson, Lona Walburn, Lori Greenburg, John DePretis, and Sheila Cobb. I would also like to express my appreciation to my mother and father for, no matter how inadvertently, giving me such a memorable childhood. Additionally, I would like to thank the real-life members of the family portrayed in this book for taking me into their home and accepting me as one of their own. I recognize that their memories of the events described in this book are different than my own. They are each fine, decent, and hard-working people. The book was not intended to hurt the family. Both my publisher and I regret any unintentional harm resulting from the publishing and marketing of Running with Scissors. Most of all, I would like to thank my brother for demonstrating, by example, the importance of being wholly unique.', NULL, 10000, 10, NULL, NULL, 1, N'031242227X', NULL)
SET IDENTITY_INSERT [dbo].[Book] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CateID], [CateName], [Description]) VALUES (1, N'
Biography', N'
Biography')
INSERT [dbo].[Category] ([CateID], [CateName], [Description]) VALUES (2, N'Business & Investing', N'Business & Investing')
INSERT [dbo].[Category] ([CateID], [CateName], [Description]) VALUES (3, N'Science & Math', N'Science & Math')
INSERT [dbo].[Category] ([CateID], [CateName], [Description]) VALUES (4, N'Humor & Entertainment
', N'Humor & Entertainment
')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Publisher] ON 

INSERT [dbo].[Publisher] ([PubID], [Name], [Description]) VALUES (1, N'Picador', N'Picador')
SET IDENTITY_INSERT [dbo].[Publisher] OFF
INSERT [dbo].[Users] ([UserName], [Password], [Email], [IsActive]) VALUES (N'ntt', N'123456', N'ntt@fsoft.com', 1)
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Author] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Author] ([AuthorID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Author]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Category] FOREIGN KEY([CateID])
REFERENCES [dbo].[Category] ([CateID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Category]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Publisher] FOREIGN KEY([PubID])
REFERENCES [dbo].[Publisher] ([PubID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Publisher]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Book] FOREIGN KEY([BookID])
REFERENCES [dbo].[Book] ([BookID])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Book]
GO
